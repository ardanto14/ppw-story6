var BRIGHT = 1
var DARK = 2

var activeTheme = BRIGHT

function changeTheme() {
    if (activeTheme == BRIGHT) {
        activeTheme = DARK;
    } else if (activeTheme == DARK) {
        activeTheme = BRIGHT;
    }
    changeCSS();
}

function changeCSS() {
    if (activeTheme == DARK) {
        var element = document.getElementById("main");
        element.style.backgroundColor = "black";
        element.style.color = "white";
        var body = document.getElementById("body");
        body.style.backgroundColor = "grey";
    } else if (activeTheme == BRIGHT) {
        var element = document.getElementById("main");
        element.style.backgroundColor = "white";
        element.style.color = "black";
        var body = document.getElementById("body");
        body.style.backgroundColor = "white";
    }
}

$(function() {
    var white = true;
    $( "#change-css" ).on( "click", function() {
        if ( white ) {
            $( ".ui-accordion-content" ).animate({
            backgroundColor: "#000",
            color: "#fff",
            }, 800 );
        } else {
            $( ".ui-accordion-content" ).animate({
              backgroundColor: "#fff",
            color: "#000",
            }, 800 );
        }
        white = !white;
    });
});