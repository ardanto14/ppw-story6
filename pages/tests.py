from django.test import TestCase, Client, LiveServerTestCase
from selenium import webdriver
from time import sleep


# Create your tests here.
class WebTest(TestCase):
    def test_template_used(self):
        Client().get("")
        self.assertTemplateUsed('home.html')

    def test_can_access_pages(self):
        response = Client().get("")
        self.assertEquals(200, response.status_code)


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        '''
        # kalo di local ini di uncomment yang atas di comment
        self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(1)
        '''

    def tearDown(self):
        self.browser.quit()

    def test_check_css_changed(self):
        self.browser.get(self.live_server_url)
        sleep(10)
        self.browser.find_element_by_id("change-css").click()
        value = self.browser.find_element_by_id("body").value_of_css_property("background-color")
        self.assertEquals("rgba(251, 251, 251, 1)", value)
